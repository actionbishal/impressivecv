import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyA9WX_qepOKOAkfRVDDHS2CKqBzg5apPok",
    authDomain: "pappalpha-adef7.firebaseapp.com",
    databaseURL: "https://pappalpha-adef7.firebaseio.com",
    projectId: "pappalpha-adef7",
    storageBucket: "pappalpha-adef7.appspot.com",
    messagingSenderId: "16379633072",
    appId: "1:16379633072:web:882528c576ddc0bf2a0a75"
});

const db = firebase.firestore()
const auth = firebase.auth()

// firebase collections
const clientsCollection = db.collection('clients')
const baseCvCollection = db.collection('baseCV')

export {
    db,
    auth,
    clientsCollection,
    baseCvCollection,
}

// export default firebaseApp.firestore();

