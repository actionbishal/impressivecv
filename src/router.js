import Vue from "vue";
import Router from "vue-router";
import Login from "./views/Login.vue";
import firebase from "firebase/app";

import MainLayout from './views/MainLayout'

require("firebase/auth");

Vue.use(Router);

let router = new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/login",
            name: "login",
            component: Login,
            meta: {
                requiresGuest: true
            }
        },
        {
            path: "/dashboard",
            name: "main-layout",
            component: MainLayout,
            meta: {
                requiresAuth: true
            },
            children: [
                {
                    path: "clients",
                    name: "clients",
                    component: () => import("./views/Dashboard.vue"),
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: "applicant/create",
                    name: "applicant",
                    props: true,
                    component: () => import("./views/Applicant.vue"),
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: "applicant/:clientId/edit",
                    name: "applicant-update",
                    props: true,
                    component: () => import("./views/ApplicantUpdate.vue"),
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: "applicant/:clientId/cv-list",
                    name: "applicant-cv-list",
                    props: true,
                    component: () => import("./views/ApplicantCvList.vue"),
                    meta: {
                        requiresAuth: true
                    }
                },
            ]
        },
        {
            path: "/cvemobile/:clid/:cvid",
            name: "cvemobile",
            component: () => import("./views/CVEmobile.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/presentation/:clientId/:clientCvId",
            name: "cvpresentation",
            props: true,
            component: () => import("./views/CVPresentation.vue"),
            meta: {
                requiresAuth: false
            }
        },
        {
            path: "/",
            redirect: {name: "login"}
        }
        /*{
          path: "/about",
          name: "about",
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () =>
            import("./views/about.vue")
        } */
    ]
});

// Navigation Guard

router.beforeEach((to, from, next) => {
    // Check for requiresAuth guard
    if (to.matched.some(record => record.meta.requiresAuth)) {

        // Check if NO logged user
        if (!firebase.auth().currentUser) {
            // Go to login
            next({
                path: "/login",
                query: {
                    redirect: {name: "login"}
                }
            });
        } else {
            // Proceed to route
            next();
        }
    } else if (to.matched.some(record => record.meta.requiresGuest)) {
        // Check if NO logged user
        if (firebase.auth().currentUser) {
            // Go to login
            next({
                path: "/dashboard/clients",
                query: {
                    redirect: {name: "clients"}
                }
            });
        } else {
            // Proceed to route
            next();
        }
    } else {
        // Proceed to route
        next();
    }
});

export default router;
