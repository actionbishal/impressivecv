import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import firebase from "firebase/app";
import $ from "jquery";
import './registerServiceWorker'

require("dotenv").config();

require("firebase/auth");
import "./plugins/firebaseInit";

import './sass/style.sass';
import store from './store'
import 'medium-editor/dist/css/medium-editor.css'
import 'medium-editor/dist/css/themes/beagle.css'
import MediumEditor from "medium-editor";
import moment from 'moment';

window.moment = moment;

window.$ = window.jQuery = require('jquery');

Vue.config.productionTip = false;

Vue.filter('monthFormat', function (value) {
    if (value)
        return moment(String(value)).format('MMM');
});

let app;
// eslint-disable-next-line
// app.use(cors())
firebase.auth().onAuthStateChanged(user => {
    if (!app) {
        /* eslint-disable no-new */
        app = new Vue({
            el: "#app",
            router,
            store,
            vuetify,
            render: h => h(App)
        });
    }
});
// $(document).ready(() => {
//     var editor = new MediumEditor('.editable', {
//         toolbar: {
//             /* These are the default options for the toolbar,
//                if nothing is passed this is what is used */
//             allowMultiParagraphSelection: true,
//             buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
//             diffLeft: 0,
//             diffTop: -10,
//             firstButtonClass: 'medium-editor-button-first',
//             lastButtonClass: 'medium-editor-button-last',
//             relativeContainer: null,
//             standardizeSelectionStart: false,
//             static: false,
//             /* options which only apply when static is true */
//             align: 'center',
//             sticky: false,
//             updateOnEmptySelection: false
//         }
//     });
// })
