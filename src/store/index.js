import Vue from 'vue'
import Vuex from 'vuex'

const fb = require('../plugins/firebaseInit.js')

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        applicant: '',
        applicantCv: [],
        applicantCvType: '',
        cvDisplaySetting: [],
        searchApplicant: '',
        searchApplicantCv: [],
        searchApplicantCvType: '',
        searchCvDisplaySetting: [],
        showSaving: false,
        snackbar: {
            showSnackbar: false,
            type: null,
            message: null
        },
        showNoneRemove: false,
        showSyncing: false
    },
    mutations: {
        applicant(state, val) {
            state.applicant = val
        },
        applicantCv(state, val) {
            state.applicantCv = val
        },
        applicantCvType(state, val) {
            state.applicantCvType = val
        },
        cvDisplaySetting(state, val) {
            state.cvDisplaySetting = val
        },
        showSaving(state, val) {
            state.showSaving = val
        },
        snackbar(state, val) {
            state.snackbar = val
        },
        showNoneRemove(state, val) {
            state.showNoneRemove = val
        },
        searchApplicant(state, val) {
            state.searchApplicant = val
        },
        searchApplicantCv(state, val) {
            state.searchApplicantCv = val
        },
        searchApplicantCvType(state, val) {
            state.searchApplicantCvType = val
        },
        searchCvDisplaySetting(state, val) {
            state.searchCvDisplaySetting = val
        },
        showSyncing(state, val) {
            state.showSyncing = val
        },
    },
    actions: {
        initRealtimeListeners(context, data) {
            fb.clientsCollection.doc(data.clientId).collection("cvlist")
                .doc(data.clientCvId).collection("cvdata").onSnapshot(snapshot => {
                snapshot.docChanges().forEach(change => {
                    if (change.type === 'modified') {
                        if (change.doc.id === 'data')
                            context.commit('applicantCv', change.doc.data());

                        if (change.doc.id === 'sections')
                            context.commit('cvDisplaySetting', change.doc.data());
                    }
                })
            });
            fb.clientsCollection.doc(data.clientId).collection("cvlist").onSnapshot(snapshot => {
                snapshot.docChanges().forEach(change => {
                    if (change.type === 'modified') {
                        context.commit('applicantCvType', change.doc.data());
                    }
                })
            });
            fb.clientsCollection.onSnapshot(snapshot => {
                snapshot.docChanges().forEach(change => {
                    if (change.type === 'modified') {
                        context.commit('applicant', change.doc.data());
                    }
                })
            });
        },
        fetchClients(context, data) {
            return new Promise((resolve, reject) => {
                fb.clientsCollection.get().then(response => {
                    resolve(response)
                }).catch(err => {
                    console.log(err)
                })
            })
        },
        fetchCurrentClient(context, data) {
            return new Promise((resolve, reject) => {
                fb.clientsCollection.doc(data.clientId).get().then(response => {
                    resolve(response)
                }).catch(err => {
                    console.log(err)
                })
            })
        },
        fetchCurrentClientCv(context, data) {
            return new Promise((resolve, reject) => {
                fb.clientsCollection.doc(data.clientId).collection("cvlist")
                    .doc(data.clientCvId).collection("cvdata")
                    .doc("data")
                    .get().then(response => {
                    resolve(response)
                }).catch(err => {
                    console.log(err)
                })
            })
        },
        fetchCurrentClientCvType(context, data) {
            return new Promise((resolve, reject) => {
                fb.clientsCollection.doc(data.clientId)
                    .collection("cvlist")
                    .doc(data.clientCvId)
                    .get().then(response => {
                    resolve(response)
                }).catch(err => {
                    console.log(err)
                })
            })
        },
        fetchCurrentClientCvSections(context, data) {
            return new Promise((resolve, reject) => {
                fb.clientsCollection.doc(data.clientId).collection("cvlist")
                    .doc(data.clientCvId).collection("cvdata")
                    .doc("sections")
                    .get().then(response => {
                    resolve(response)
                }).catch(err => {
                    console.log(err)
                })
            })
        },
        fetchApplicantCvs({commit, state}, data) {
            return new Promise((resolve, reject) => {
                fb.clientsCollection.doc(data.clientId).collection("cvlist").get()
                    .then(response => {
                        resolve(response)
                    }).catch(err => {
                    console.log(err)
                })
            })
        },
        getAllClient({commit, state}, data) {
            return new Promise((resolve, reject) => {
                fb.clientsCollection.get()
                    .then(response => {
                        resolve(response)
                    }).catch(err => {
                    console.log(err)
                })
            })
        },
        removeCv({commit, state}, data) {
            fb.clientsCollection.doc(data.clientId).collection("cvlist")
                .doc(data.clientCvId).delete()
        },
        removeClient({commit, state}, data) {
            fb.clientsCollection.doc(data.clientId).delete();
        },
        storeCvType({commit, state}, data) {
            fb.clientsCollection.doc(data.clientId)
                .collection("cvlist")
                .doc(data.clientCvId)
                .update(data.applicantCvType).then((res) => {
            }).catch(err => {
                console.log(err)
            }).finally(() => {
                setTimeout(() => {
                    commit('showSaving', false)
                }, 3000)
            })
        },
        storeCvData({commit, state}, data) {
            fb.clientsCollection.doc(data.clientId).collection("cvlist")
                .doc(data.clientCvId).collection("cvdata")
                .doc("data")
                .set(data.applicantCv).then((res) => {
            }).catch(err => {
                console.log(err)
            }).finally(() => {
                setTimeout(() => {
                    commit('showSaving', false)
                }, 3000)
            })
        },
        storeCvDisplaySetting({commit, state}, data) {
            fb.clientsCollection.doc(data.clientId).collection("cvlist")
                .doc(data.clientCvId).collection("cvdata")
                .doc("sections")
                .set(data.cvDisplaySetting).then((res) => {
            }).catch(err => {
                console.log(err)
            }).finally(() => {
                setTimeout(() => {
                    commit('showSaving', false)
                }, 3000)
            })
        },
        updateApplicant({commit, state}, data) {
            return new Promise((resolve, reject) => {
                fb.clientsCollection.doc(data.clientId).update(data.applicant)
                    .finally(() => {
                        commit('showSaving', false)
                    });
            })
        },
        updateCvDataSpecific({commit, state}, data) {
            return new Promise((resolve, reject) => {
                fb.clientsCollection.doc(data.clientId).collection("cvlist")
                    .doc(data.clientCvId).collection("cvdata")
                    .doc("data")
                    .update({[data.type]: data.cvData}).then((res) => {
                }).catch(err => {
                    console.log(err)
                }).finally(() => {
                    setTimeout(() => {
                        commit('showSaving', false)
                    }, 3000)
                })
            })
        }
    },
    getters: {
        applicant: state => state.applicant,
        applicantCv: state => state.applicantCv,
        applicantCvType: state => state.applicantCvType,
        cvDisplaySetting: state => state.cvDisplaySetting,
        showSaving: state => state.showSaving,
        showSnackbar: state => state.showSnackbar,
        showNoneRemove: state => state.showNoneRemove,
        searchApplicant: state => state.searchApplicant,
        searchApplicantCv: state => state.searchApplicantCv,
        searchApplicantCvType: state => state.searchApplicantCvType,
        searchCvDisplaySetting: state => state.searchCvDisplaySetting,
        showSyncing: state => state.showSyncing,
    }
})
