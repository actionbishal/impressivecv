const path = require('path')

module.exports = {
    assetsDir: 'assets/',
    pwa: {
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            swSrc: './src/sw.js',
            swDest: 'service-worker.js',
        },
    },
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@/sass/style.sass";`
            }
        }
    },
    // chainWebpack: config => {
    //     config.module
    //         .rule("fonts")
    //         .test(/\.(ttf|otf|eot|woff|woff2)$/)
    //         .use("file-loader")
    //         .loader("file-loader")
    //         .tap(options => {
    //             options = {
    //                 // limit: 10000,
    //                 name: '/assets/fonts/[name].[ext]',
    //             }
    //             return options
    //         })
    //         .end()
    // }
}

